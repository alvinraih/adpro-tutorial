package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class TomatoSauce extends Filling {
    Food food;
    
    public TomatoSauce(Food food) {
        this.food = food;
    }
    
    @Override
    public String getDescription() {
        // TODO Auto-generated method stub
        return food.getDescription() + ", adding tomato sauce";
    }

    @Override
    public double cost() {
        // TODO Auto-generated method stub
        return food.cost() + 0.2;
    }

}
