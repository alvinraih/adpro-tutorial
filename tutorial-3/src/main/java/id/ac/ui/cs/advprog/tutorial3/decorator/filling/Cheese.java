package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Cheese extends Filling {
    Food food;
    
    public Cheese(Food food) {
        this.food = food;
    }

    @Override
    public String getDescription() {
        // TODO Auto-generated method stub
        return food.getDescription() + ", adding cheese";
    }
    
    @Override
    public double cost() {
        // TODO Auto-generated method stub
        return food.cost() + 2.0;
    }

}
