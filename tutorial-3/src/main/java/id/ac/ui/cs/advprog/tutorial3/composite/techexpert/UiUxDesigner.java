package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class UiUxDesigner extends Employees {

    public UiUxDesigner(String name, double salary) {
        if (salary < 90000.00) {
            throw new IllegalArgumentException("UiUxDesigner Salary "
                    + "must not lower than 90000.00");
        } else {
            this.name = name;
            this.salary = salary;
            this.role = "UI/UX Designer";
        }
    }

    @Override
    public double getSalary() {
        // TODO Auto-generated method stub
        return this.salary;
    }

}
